use apatite::app::{App, AppSettings};

use apatite_controler::{ActionTrigger, Controler, ControlerAction, KeyCode};

use legion::{system, Schedule};

pub fn main() -> Result<(), Box<dyn std::error::Error>> {
    App::new().run(AppSettings {
        window_attributes: Some(Default::default()),
        timer: Default::default(),
        fps_limiter: Default::default(),
        controller_bundle: Controler::new(&[
            ControlerAction {
                name: Action::Forward,
                triggers: &[ActionTrigger::Key(KeyCode::W)],
            },
            ControlerAction {
                name: Action::Jump,
                triggers: &[ActionTrigger::Key(KeyCode::Space)],
            },
        ])
        .into(),
        systems: Schedule::builder()
            .add_system(print_system())
            .add_system(print_constantly_system())
            .build(),
    })
}

#[system]
fn print(#[resource] controler: &Controler<Action>) {
    if controler.just_pressed(&Action::Jump) {
        println!("jumped");
    }
}

#[system]
fn print_constantly(#[resource] controler: &Controler<Action>) {
    if controler.pressed(&Action::Forward) {
        println!("running");
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
enum Action {
    Jump,
    Forward,
}
