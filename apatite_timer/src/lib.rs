use apatite_bundle::Bundle;
use legion::{system, systems::Builder, Resources};

use std::num::NonZeroU16;
use std::time::{Duration, Instant};

#[derive(Debug)]
pub struct Timer {
    last_cycle: Option<Instant>,
    last_subcycle: Option<Instant>,
    cycle: Duration,
}

/// Creating new Timer
impl Timer {
    pub fn new(cps: NonZeroU16) -> Self {
        Self {
            last_cycle: None,
            last_subcycle: None,
            cycle: Duration::from_secs_f64(1.0 / cps.get() as f64),
        }
    }
}

/// Timer cycle updating
impl Timer {
    pub fn start_cycle(&mut self) {
        self.last_cycle = Some(Instant::now());
    }

    pub fn start_subcycle(&mut self) {
        self.last_subcycle = Some(Instant::now());
    }
}

/// Getting time information
impl Timer {
    pub fn dt(&self) -> f64 {
        self.cycle.as_secs_f64()
    }

    pub fn since_cycle(&self) -> Duration {
        if let Some(last_cycle) = self.last_cycle {
            if let Some(last_subcycle) = self.last_subcycle {
                if last_subcycle >= last_cycle {
                    last_subcycle.duration_since(last_cycle)
                } else {
                    Duration::new(0, 0)
                }
            } else {
                Duration::new(0, 0)
            }
        } else {
            Duration::new(0, 0)
        }
    }

    pub fn cycle_progress(&self) -> f64 {
        self.since_cycle().as_secs_f64() / self.cycle.as_secs_f64()
    }

    pub fn cycle_ready(&self) -> bool {
        if let Some(last_cycle) = self.last_cycle {
            Instant::now().duration_since(last_cycle) >= self.cycle
        } else {
            true
        }
    }
}

impl Default for Timer {
    fn default() -> Self {
        Self::new(NonZeroU16::new(60).unwrap())
    }
}

#[system]
fn timer_start_cycle(#[resource] timer: &mut Timer) {
    timer.start_cycle()
}

#[system]
fn timer_start_subcycle(#[resource] timer: &mut Timer) {
    timer.start_subcycle()
}

pub struct TimerBundle {
    timer: Timer,
}

impl TimerBundle {
    pub fn new(timer: Timer) -> Self {
        Self { timer }
    }
}

impl Bundle for TimerBundle {
    fn insert(
        self,
        resources: &mut Resources,
        early_subcycle_systems: &mut Builder,
        early_cycle_systems: &mut Builder,
        _late_cycle_systems: &mut Builder,
        _late_subcycle_systems: &mut Builder,
    ) {
        resources.insert(self.timer);
        early_subcycle_systems.add_system(timer_start_subcycle_system());
        early_cycle_systems.add_system(timer_start_cycle_system());
    }
}
