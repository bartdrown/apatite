//! The renderer context, created once during initialization.

use std::{error::Error, sync::Arc};

use futures::executor;

use wgpu::{
    BackendBit, Device, DeviceDescriptor, Features, Instance, Limits, PowerPreference, Queue,
    RequestAdapterOptions, Surface,
};

use winit::window::Window;

#[derive(Debug, Clone)]
pub struct RendererContext {
    device: Arc<Device>,
    queue: Arc<Queue>,
    window: Arc<Window>,
    surface: Arc<Surface>,
}

impl RendererContext {
    /// Initialize the graphics context, create a window.
    pub fn new(window: Window) -> Result<Self, Box<dyn Error>> {
        let instance = Instance::new(BackendBit::PRIMARY);
        let surface = unsafe { instance.create_surface(&window) };
        let (device, queue) = create_device(&instance, &surface)?;

        Ok(Self {
            device: Arc::new(device),
            queue: Arc::new(queue),
            window: Arc::new(window),
            surface: Arc::new(surface),
        })
    }

    pub fn device(&self) -> &Device {
        self.device.as_ref()
    }

    pub fn queue(&self) -> &Queue {
        self.queue.as_ref()
    }

    pub fn window(&self) -> &Window {
        self.window.as_ref()
    }

    pub fn surface(&self) -> &Surface {
        self.surface.as_ref()
    }
}

fn create_device(
    instance: &Instance,
    surface: &Surface,
) -> Result<(Device, Queue), Box<dyn Error>> {
    let adapter = executor::block_on(instance.request_adapter(&RequestAdapterOptions {
        power_preference: PowerPreference::HighPerformance,
        compatible_surface: Some(surface),
    }))
    .ok_or("No sufficient graphics device found")?;

    let required_limits = Limits {
        max_push_constant_size: 128,
        ..Limits::default()
    };
    let required_features = Features::PUSH_CONSTANTS;

    if !adapter.features().contains(required_features) {
        return Err("No sufficient graphics device found".into());
    }

    let (device, queue) = executor::block_on(adapter.request_device(
        &DeviceDescriptor {
            features: required_features,
            limits: required_limits,
            shader_validation: true,
        },
        None,
    ))?;

    Ok((device, queue))
}
