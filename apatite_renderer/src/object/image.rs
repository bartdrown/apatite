use super::Object;
use crate::default::RenderingDefaults;
use bytemuck::{Pod, Zeroable};
use std::{error::Error, mem, path::Path, sync::Arc};
use wgpu::{
    util::BufferInitDescriptor, util::DeviceExt, BindGroup, Buffer, BufferAddress, BufferUsage,
    Device, Extent3d, InputStepMode, Origin3d, Queue, RenderPipeline, Sampler, Texture,
    TextureDescriptor, TextureDimension, TextureFormat, TextureUsage, VertexAttributeDescriptor,
    VertexBufferDescriptor, VertexFormat,
};

#[derive(Debug, Clone)]
pub struct Image {
    pipeline: Arc<RenderPipeline>,
    buffer: Arc<Buffer>,
    bind_group: Arc<BindGroup>,
}

impl Image {
    pub fn new(
        device: &Device,
        defaults: &RenderingDefaults,
        texture: &Texture,
        pipeline: Arc<RenderPipeline>,
        sampler: &Sampler,
    ) -> Self {
        let buffer = image_buffer(device).into();
        Self {
            pipeline,
            buffer,
            bind_group: defaults
                .bind_group_layouts
                .image_bind_group(device, texture, sampler)
                .into(),
        }
    }

    pub fn new_default(device: &Device, defaults: &RenderingDefaults, texture: &Texture) -> Self {
        let buffer = image_buffer(device).into();
        Self {
            pipeline: defaults.pipelines.image.clone(),
            buffer,
            bind_group: defaults
                .bind_group_layouts
                .image_bind_group(device, texture, &defaults.samplers.image)
                .into(),
        }
    }
}

fn image_buffer(device: &Device) -> Buffer {
    let vertices = &[
        ImageVertex {
            position: [-1.0, -1.0],
            uv: [0.0, 1.0],
        },
        ImageVertex {
            position: [-1.0, 1.0],
            uv: [0.0, 0.0],
        },
        ImageVertex {
            position: [1.0, -1.0],
            uv: [1.0, 1.0],
        },
        ImageVertex {
            position: [1.0, 1.0],
            uv: [1.0, 0.0],
        },
    ];
    device.create_buffer_init(&BufferInitDescriptor {
        label: Some("image vertex buffer"),
        contents: bytemuck::cast_slice(vertices),
        usage: BufferUsage::VERTEX,
    })
}

pub fn load_texture(
    device: &Device,
    queue: &Queue,
    path: &impl AsRef<Path>,
) -> Result<Texture, Box<dyn Error>> {
    let image = image::open(path)?.to_rgba();
    let dim = image.dimensions();
    let texture = create_texture(device, queue, &image, [dim.0, dim.1]);
    Ok(texture)
}

pub fn create_texture(device: &Device, queue: &Queue, data: &[u8], size: [u32; 2]) -> Texture {
    let size = Extent3d {
        width: size[0],
        height: size[1],
        depth: 1,
    };

    let texture = device.create_texture(&TextureDescriptor {
        label: Some("Image texture"),
        size,
        mip_level_count: 1,
        sample_count: 1,
        dimension: TextureDimension::D2,
        format: TextureFormat::Rgba8UnormSrgb,
        usage: TextureUsage::SAMPLED | TextureUsage::COPY_DST,
    });

    queue.write_texture(
        wgpu::TextureCopyView {
            texture: &texture,
            mip_level: 0,
            origin: Origin3d::ZERO,
        },
        data,
        wgpu::TextureDataLayout {
            offset: 0,
            bytes_per_row: 4 * size.width,
            rows_per_image: size.height,
        },
        size,
    );

    texture
}

impl Object for Image {
    fn pipeline(&self) -> &RenderPipeline {
        &self.pipeline
    }

    fn buffer(&self) -> &Buffer {
        &self.buffer
    }

    fn buffer_len(&self) -> u32 {
        4
    }

    fn bind_group(&self) -> Option<&BindGroup> {
        Some(&self.bind_group)
    }
}

#[derive(Debug, Copy, Clone, Pod, Zeroable)]
#[repr(C)]
pub struct ImageVertex {
    pub position: [f32; 2],
    pub uv: [f32; 2],
}

impl ImageVertex {
    pub fn vertex_buffer_descriptor() -> VertexBufferDescriptor<'static> {
        VertexBufferDescriptor {
            stride: mem::size_of::<ImageVertex>() as BufferAddress,
            step_mode: InputStepMode::Vertex,
            attributes: &[
                VertexAttributeDescriptor {
                    offset: 0,
                    format: VertexFormat::Float2,
                    shader_location: 0,
                },
                VertexAttributeDescriptor {
                    offset: mem::size_of::<[f32; 2]>() as BufferAddress,
                    format: VertexFormat::Float2,
                    shader_location: 1,
                },
            ],
        }
    }
}
