use shaderc::{Compiler, ShaderKind};
use std::{error::Error, sync::Arc};
use wgpu::{Device, ShaderModule};

mod image;

#[derive(Clone, Debug)]
pub struct DefaultShaders {
    pub image_vertex: Arc<ShaderModule>,
    pub image_fragment: Arc<ShaderModule>,
}

impl DefaultShaders {
    pub fn new(device: &Device) -> Result<Self, Box<dyn Error>> {
        let mut compiler = ShaderCompiler::new(device)?;
        Ok(Self {
            image_vertex: Arc::new(compiler.compile_shader(
                image::DEFAULT_VERTEX,
                ShaderKind::Vertex,
                "default_image.vert",
            )?),
            image_fragment: Arc::new(compiler.compile_shader(
                image::DEFAULT_FRAGMENT,
                ShaderKind::Fragment,
                "default_image.frag",
            )?),
        })
    }
}

/// Interface to the shader compiler.
struct ShaderCompiler<'a> {
    compiler: Compiler,
    device: &'a Device,
}

impl<'a> ShaderCompiler<'a> {
    pub fn new(device: &'a Device) -> Result<Self, Box<dyn Error>> {
        let compiler = Compiler::new().ok_or("Failed to initialize the shader compiler")?;
        Ok(Self { compiler, device })
    }

    pub fn compile_shader(
        &mut self,
        src: &str,
        kind: ShaderKind,
        filename: &str,
    ) -> Result<ShaderModule, Box<dyn Error>> {
        Ok(self.device.create_shader_module(wgpu::util::make_spirv(
            self.compiler
                .compile_into_spirv(src, kind, filename, "main", None)?
                .as_binary_u8(),
        )))
    }
}
