use crate::bind_group::DefaultBindGroupLayouts;
use crate::object::image::ImageVertex;
use crate::shader::DefaultShaders;
use std::sync::Arc;
use wgpu::{
    BlendDescriptor, ColorStateDescriptor, ColorWrite, CompareFunction, CullMode,
    DepthStencilStateDescriptor, Device, FrontFace, IndexFormat, PipelineLayoutDescriptor,
    PrimitiveTopology, ProgrammableStageDescriptor, RasterizationStateDescriptor, RenderPipeline,
    RenderPipelineDescriptor, StencilStateDescriptor, StencilStateFaceDescriptor,
    VertexStateDescriptor,
};

#[derive(Debug, Clone)]
pub struct DefaultPipelines {
    pub image: Arc<RenderPipeline>,
}

impl DefaultPipelines {
    pub fn new(
        device: &Device,
        default_bind_group_layouts: &DefaultBindGroupLayouts,
        default_shaders: &DefaultShaders,
    ) -> Self {
        Self {
            image: Arc::new(default_image_pipeline(
                device,
                default_bind_group_layouts,
                default_shaders,
            )),
        }
    }
}

fn default_image_pipeline(
    device: &Device,
    default_bind_group_layouts: &DefaultBindGroupLayouts,
    default_shaders: &DefaultShaders,
) -> RenderPipeline {
    let layout = device.create_pipeline_layout(&PipelineLayoutDescriptor {
        label: Some("Default image pipeline layout"),
        bind_group_layouts: &[&default_bind_group_layouts.image],
        push_constant_ranges: &[],
    });
    device.create_render_pipeline(&RenderPipelineDescriptor {
        label: Some("Default image pipeline"),
        layout: Some(&layout),
        vertex_stage: ProgrammableStageDescriptor {
            module: &default_shaders.image_vertex,
            entry_point: "main",
        },
        fragment_stage: Some(ProgrammableStageDescriptor {
            module: &default_shaders.image_fragment,
            entry_point: "main",
        }),
        rasterization_state: Some(RasterizationStateDescriptor {
            front_face: FrontFace::Cw,
            cull_mode: CullMode::None,
            clamp_depth: false,
            depth_bias: 0,
            depth_bias_clamp: 0.0,
            depth_bias_slope_scale: 0.0,
        }),
        primitive_topology: PrimitiveTopology::TriangleStrip,
        color_states: &[ColorStateDescriptor {
            format: crate::SWAPCHAIN_TEXTURE_FORMAT,
            alpha_blend: BlendDescriptor::REPLACE,
            color_blend: BlendDescriptor::REPLACE,
            write_mask: ColorWrite::ALL,
        }],
        depth_stencil_state: Some(DepthStencilStateDescriptor {
            format: crate::DEPTH_TEXTURE_FORMAT,
            depth_write_enabled: false,
            depth_compare: CompareFunction::Always,
            stencil: StencilStateDescriptor {
                front: StencilStateFaceDescriptor::IGNORE,
                back: StencilStateFaceDescriptor::IGNORE,
                read_mask: 0,
                write_mask: 0,
            },
        }),
        vertex_state: VertexStateDescriptor {
            index_format: IndexFormat::Uint16,
            vertex_buffers: &[ImageVertex::vertex_buffer_descriptor()],
        },
        sample_count: 1,
        sample_mask: !0,
        alpha_to_coverage_enabled: false,
    })
}
