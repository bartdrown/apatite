//! Apatite Renderer

use std::error::Error;

use wgpu::{
    Color, CommandEncoderDescriptor, Device, Extent3d, LoadOp, Operations, PresentMode,
    RenderPassColorAttachmentDescriptor, RenderPassDepthStencilAttachmentDescriptor,
    RenderPassDescriptor, Surface, SwapChain, SwapChainDescriptor, SwapChainError, Texture,
    TextureAspect, TextureDescriptor, TextureDimension, TextureFormat, TextureUsage, TextureView,
    TextureViewDescriptor, TextureViewDimension,
};
use winit::dpi::PhysicalSize;

use context::RendererContext;
use default::RenderingDefaults;
use object::Object;

pub mod bind_group;
pub mod context;
pub mod default;
pub mod object;
pub mod pipeline;
pub mod sampler;
pub mod shader;

const SWAPCHAIN_TEXTURE_FORMAT: TextureFormat = TextureFormat::Bgra8UnormSrgb;

const DEPTH_TEXTURE_FORMAT: TextureFormat = TextureFormat::Depth32Float;

/// The interface to all rendering operations.
#[derive(Debug)]
pub struct Renderer {
    /// The renderer context. This should be created once and never change.
    context: RendererContext,
    /// The swapchain that we are rendering to.
    swapchain: SwapChain,
    /// The size of the window surface that we are rendering to.
    size: PhysicalSize<u32>,
    /// Should the swapchain be recreated before we draw the next frame.
    recreate_swapchain: bool,
    /// The depth buffer.
    depth_texture: Texture,
    /// Default resources for rendering
    defaults: RenderingDefaults,
}

impl Renderer {
    pub fn new(context: RendererContext) -> Result<Self, Box<dyn Error>> {
        let size = context.window().inner_size();
        let swapchain = create_swapchain(context.device(), context.surface(), size);
        let depth_texture = create_depth_texture(context.device(), size);
        let defaults = RenderingDefaults::new(context.device())?;

        Ok(Self {
            context,
            swapchain,
            size,
            recreate_swapchain: false,
            depth_texture,
            defaults,
        })
    }

    pub fn draw_frame(&mut self, objects: &[&dyn Object]) -> Result<(), Box<dyn Error>> {
        if self.recreate_swapchain {
            self.swapchain =
                create_swapchain(&self.context.device(), self.context.surface(), self.size);
            self.depth_texture = create_depth_texture(self.context.device(), self.size);
            self.recreate_swapchain = false;
        }

        let output_texture = match self.swapchain.get_current_frame() {
            Ok(frame) => {
                if frame.suboptimal {
                    let new_size = self.context.window().inner_size();
                    self.resize(new_size);
                    return Ok(());
                } else {
                    frame.output
                }
            }
            Err(SwapChainError::Outdated) | Err(SwapChainError::Lost) => {
                let new_size = self.context.window().inner_size();
                self.resize(new_size);
                return Ok(());
            }
            Err(err) => {
                return Err(err.into());
            }
        };

        let mut encoder = self
            .context
            .device()
            .create_command_encoder(&CommandEncoderDescriptor {
                label: Some("Render encoder"),
            });

        {
            let depth_texture_view = create_depth_texture_view(&self.depth_texture);
            let mut render_pass = encoder.begin_render_pass(&RenderPassDescriptor {
                color_attachments: &[RenderPassColorAttachmentDescriptor {
                    attachment: &output_texture.view,
                    resolve_target: None,
                    ops: Operations {
                        load: LoadOp::Clear(Color {
                            r: 0.0,
                            g: 0.0,
                            b: 0.0,
                            a: 1.0,
                        }),
                        store: true,
                    },
                }],
                depth_stencil_attachment: Some(RenderPassDepthStencilAttachmentDescriptor {
                    attachment: &depth_texture_view,
                    depth_ops: Some(Operations {
                        load: LoadOp::Clear(1.0),
                        store: true,
                    }),
                    stencil_ops: None,
                }),
            });
            for object in objects {
                render_pass.set_pipeline(object.pipeline());
                if let Some(bind_group) = object.bind_group() {
                    render_pass.set_bind_group(0, bind_group, &[]);
                }
                render_pass.set_vertex_buffer(0, object.buffer().slice(..));
                render_pass.draw(0..object.buffer_len(), 0..1);
            }
        }

        self.context.queue().submit(Some(encoder.finish()));

        Ok(())
    }

    pub fn resize(&mut self, new_size: PhysicalSize<u32>) {
        self.size = new_size;
        self.recreate_swapchain = true;
    }

    pub fn context(&self) -> &RendererContext {
        &self.context
    }

    pub fn defaults(&self) -> &RenderingDefaults {
        &self.defaults
    }
}

/// Creates a swapchain. Also used for swapchain recreation.
fn create_swapchain(device: &Device, surface: &Surface, size: PhysicalSize<u32>) -> SwapChain {
    device.create_swap_chain(
        surface,
        &SwapChainDescriptor {
            usage: TextureUsage::OUTPUT_ATTACHMENT,
            format: SWAPCHAIN_TEXTURE_FORMAT,
            width: size.width,
            height: size.height,
            present_mode: PresentMode::Fifo,
        },
    )
}

/// Creates the depth buffer.
fn create_depth_texture(device: &Device, size: PhysicalSize<u32>) -> Texture {
    device.create_texture(&TextureDescriptor {
        label: Some("depth buffer texture"),
        size: Extent3d {
            width: size.width,
            height: size.height,
            depth: 1,
        },
        mip_level_count: 1,
        sample_count: 1,
        dimension: TextureDimension::D2,
        format: DEPTH_TEXTURE_FORMAT,
        usage: TextureUsage::OUTPUT_ATTACHMENT,
    })
}

fn create_depth_texture_view(texture: &Texture) -> TextureView {
    texture.create_view(&TextureViewDescriptor {
        label: Some("depth texture view"),
        format: Some(DEPTH_TEXTURE_FORMAT),
        dimension: Some(TextureViewDimension::D2),
        aspect: TextureAspect::DepthOnly,
        base_mip_level: 0,
        level_count: None,
        base_array_layer: 0,
        array_layer_count: None,
    })
}
